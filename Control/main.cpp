#include <curses.h>
#include <psensor.h>
#include <vector>
#include <algorithm>
#include <thread>
#include "taskFunctions.h"
#include "fsm.h"
#include "unistd.h"

using namespace std;

// globals
int sensorNum = 0;
int measNum = 0;
int rawSensorData[5][6];
int medEntry = 2; // because the median of {0, 4} is 2
int threshold = 50;
int parallelSleep = 50000;

int executionCount = 0;

vector<vector<bool> > logicalData(6, vector<bool>(5));
volatile bool medValues[6];
volatile bool pastValues[6];
bool offFlag = false;
bool changeFlag[6] = {false, false, false, false, false, false};
bool newData = false;
key sensorInput;
iRobot robot;

bool taskFlag = false;
extern state present_state;

// callback function for keyboard scanning thread
void kbScanFcn()
{
    int dir;
    while(!offFlag){
        dir = getch();
        if (dir == 'o'){
            cout << "==================================== \n\r";
            cout << "goodbye \n\r";
            cout << "==================================== \n\r";
            robot.stop();
            offFlag = true;
            for (int i = 0; i < 6; i++){
                medValues[i] &= 0x00;
            }
        }
    }
}

int main()
{

    std::string ip;
    ip = "192.168.0.100";

    // make sure connection is successful
    if (robot.startNetwork(ip,2468)!=ERROR::NONE){
        std::cout << "No network path found \n";
        return -1;
    }
    robot.modefull();

    // callback lambda
    robot.registerCallback({SENSOR::bumpleft, SENSOR::bumpfleft, SENSOR::bumpcleft, SENSOR::bumpcright, SENSOR::bumpfright, SENSOR::bumpright},
                           [](std::shared_ptr<pSensor> data){
        int32_t din;

        // make sure our indices are in the correct range
        sensorNum = sensorNum%6;
        measNum = measNum%5;

        // check if data is valid
        if (data->getData(din)!=ERROR::NONE){
            std::cout << "\n\rData not valid \n";
        }

        // collect data
        else{

            // if out of sync, reset sensorNum
            if (data->getType() == SENSOR::bumpleft){
                sensorNum = 0;
            }

            // save to raw array (currently unused)
            rawSensorData[measNum][sensorNum] = din;

            // save data into logical array
            if (din > threshold){
                logicalData[sensorNum][measNum] = true;
            }
            else {
                logicalData[sensorNum][measNum] = false;

            }

            //cout << logicalData[sensorNum][measNum] << "\t";    // constant update

            // if all 6 sensors have been read
            if (sensorNum == 5){
                // increment measurement number
                measNum++;
                //std::cout << "\n\r";
            }

            // if all 5 measurements have been taken, sort array then take median
            if (measNum == 5){
                newData = false;
                // sort
                for (size_t i = 0; i != logicalData.size(); i++){
                    sort(logicalData[i].begin(), logicalData[i].end());
                    pastValues[i] = medValues[i];
                    medValues[i] = logicalData[i][medEntry];
                    if (pastValues[i] != medValues[i]){
                        changeFlag[i] = true;
                        //cout << "data changed \n\r";
                    } else {
                        changeFlag[i] = false;
                        //cout << "data hasn't changed \n\r";
                    }
                    //cout << medValues[i] << "\t";
                }
                for (int i = 0; i < 6; i++){
                    newData = newData | changeFlag[i];
                }

                // if change in data, interpret input
                if (newData){
                    //if //((medValues[0] | medValues[1])){
                        //sensorInput = bothLeft;
                    //} else
                    if ((medValues[2] | medValues[3]) & (medValues[4] | medValues[5])){
                        sensorInput = front_right;
                    } else if ((medValues[2] | medValues[3]) & !(medValues[4] | medValues[5]) ){
                        sensorInput = front_NOTright;
                    } else if (!(medValues[2] | medValues[3]) & (medValues[4] | medValues[5])){
                        sensorInput = NOTfront_right;
                    } else if (!(medValues[2] | medValues[3] | medValues[4]) & medValues[5]){
                        sensorInput = onlyFarRight;
                    } else if (!(medValues[2] | medValues[3]) & !(medValues[4] | medValues[5])){
                        sensorInput = NOTfront_NOTright;
                    }
                }

            }

            // increment to next sensor
            sensorNum++;
        }

    });

    // configure constant stream of data
    robot.sstream({SENSOR::bumpleft, SENSOR::bumpfleft, SENSOR::bumpcleft, SENSOR::bumpcright, SENSOR::bumpfright, SENSOR::bumpright});

    // begin sensor stream
    robot.sensorStart();

    // start keyboard scanning thread
    std::thread threadObj(kbScanFcn);

    // some curses stuff
    initscr();
    timeout(-1);
    raw();
    cbreak();
    noecho();

    std::cout << "Initially driving straight until I hit a wall. \n\r";
    robot.dStraight(50); // have to make it move initially
    //sleep(5);

    // we need a process to continuously run (infinite while is good)
    // the main thread will continously poll for new data
    while (!offFlag){
/*
        if (present_state == getParallel){
            //getToParallel();
            //usleep(parallelSleep);
            fsm(getParallel, parallelFlag);
            //sensorInput = parallelFlag;
            //newData = true;
        }
        //else if (newData){
        else
*/
        if(newData){
            std::cout << "present_state = " << present_state << "\n\r";
            std::cout << "sensorInput = " << sensorInput << "\n\r";


            fsm(present_state, sensorInput);

            std::cout << executionCount << "\n\r";
            executionCount++;

            //newData = false;
        }
        //else {

        //}
    }

    // join keyboard scanning thread with main thread
    threadObj.join();
}
