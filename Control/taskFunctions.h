#ifndef TASKFUNCTIONS
#define TASKFUNCTIONS

#include <iostream>
#include "irobot.h"

//void rotateNinetyCCW();
void rotateUntilNoData();
void getToParallel();
void driveStraight();
void awayFromWall();
void startLeaving();
void completelyOut();

void leaveBox();
void null_fcn();

#endif // TASKFUNCTIONS

