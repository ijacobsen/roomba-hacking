#include "fsm.h"

state present_state = startBot;

const transition startBot_transitions[] = {
    // input            next state      task
    //{NOTfront_NOTright, startBot,       null_fcn},
    {bothLeft,          turning,        rotateUntilNoData},
    {front_NOTright,    turning,        rotateUntilNoData},
    {front_right,       turning,        rotateUntilNoData},
    {NOTfront_right,    turning,        rotateUntilNoData},
    {eol,               startBot,       null_fcn}
};

const transition turning_transitions[] = {
    // input            next state      task
    //{NOTfront_NOTright, getParallel,    getToParallel},
    {NOTfront_NOTright, wallCrawl,      awayFromWall},
    {eol,               turning,        null_fcn}
};

/*
const transition getParallel_transitions[] = {
    // input            next state      task
    {parallelFlag,      wallCrawl,      driveStraight}
    //{eol,               getParallel,    null_fcn}
};


const transition hitWall_transitions[] = {
    // input            next state      task
    {NOTfront_right,    wallCrawl,      driveStraight},
    {NOTfront_NOTright, wallCrawl,      driveStraight},
    {eol,               hitWall,        null_fcn}
};
*/

const transition wallCrawl_transitions[] = {
    // input            next state      task
    //{front_right,     hitWall,        rotateNinetyCCW},

    {onlyFarRight,      wallCrawl,      awayFromWall},
    //{NOTfront_right,    turning,        rotateUntilNoData},
    {front_right,       turning,        rotateUntilNoData},
    //{front_NOTright,    turning,        rotateUntilNoData},
    {NOTfront_NOTright, leaving,        startLeaving},
    {eol,               wallCrawl,      null_fcn}
};

const transition leaving_transitions[] = {
    // input            next state      task
    {onlyFarRight,      wallCrawl,      awayFromWall},
    {front_NOTright,    turning,        rotateUntilNoData},
    {NOTfront_right,    wallCrawl,      awayFromWall},
    {front_right,       turning,        rotateUntilNoData},
    {timeoutFlag,       out,            completelyOut},
    {eol,               leaving,        null_fcn}
};

const transition out_transitions[] = {
    // input            next state      task
    {eol,               out,            null_fcn}
};

/*
const transition foundExit_transitions[] = {
    // input         next state      task
    {eol,            foundExit,      null_fcn}
};
*/

const transition* ps_transitions_ptr[]={
    /*
    startBot_transitions,
    hitWall_transitions,
    wallCrawl_transitions,
    foundExit_transitions
    */
    startBot_transitions,
    turning_transitions,
    //getParallel_transitions,
    wallCrawl_transitions,
    leaving_transitions,
    out_transitions

};

void fsm (state ps, key keyval){
    int i;

    for(i = 0; (ps_transitions_ptr[ps][i].keyval != keyval) && (ps_transitions_ptr[ps][i].keyval != eol); i++);

    ps_transitions_ptr[ps][i].tf_ptr();

    present_state = ps_transitions_ptr[ps][i].next_state;
}
