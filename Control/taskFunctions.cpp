#include "taskFunctions.h"
#include "unistd.h"
extern iRobot robot;

int parallelTime = 50000;

/*
void rotateNinetyCCW(){
    std::cout << "hit a wall, now turning counter clockwise \n\r";
    robot.dCClockwise(50);
    sleep(3);
    std::cout << "finished turning... stopping and waiting \n\r";
    robot.dStraight(0);

}
*/

void rotateUntilNoData(){
    std::cout << "Hit a wall. Rotating CCW until no more data... \n\r";
    robot.dCClockwise(100);
}

void getToParallel(){
    std::cout << "No more data. Finishing turn to get parallel. \n\r";
    //robot.dCClockwise(0);
    //usleep(parallelTime);
    robot.dCClockwise(0);
    //sleep(3);
}

void driveStraight(){
    std::cout << "Driving straight until I hit a wall or detect an exit. \n\r";
    robot.dStraight(100);
}

void awayFromWall(){
    std::cout << "Moving away from wall. \n\r";

    //robot.dStraight(100);
    //sleep(3);
    //robot.dClockwise(50);
    //sleep(3);
    std::cout << "Driving until I am out of the box or hit another wall. \n\r";
    robot.driveDirect(100,50);
    //robot.dStraight(100);

}

void towardWall(){
    std::cout << "Moving toward wall. \n\r";

    robot.driveDirect(180,300);
}

void startLeaving(){
    std::cout << "Starting to leave now. \n\r";

    //robot.dStraight(100);
    //sleep(3);
    //robot.dClockwise(50);
    //sleep(3);
    std::cout << "Driving until I am out of the box or hit another wall. \n\r";
    robot.driveDirect(50,100);
    //robot.dStraight(100);

}

void completelyOut(){
    std::cout << "Completely out. Turn me off please. \n\r";
    robot.dStraight(0);
}

void leaveBox(){
    std::cout << "found exit, now leaving \n\r";
    robot.dStraight(100);
    sleep(1);
    std::cout << "turning";
    robot.dClockwise(50);
    sleep(3);
    std::cout << "driving out of the exit \n\r";
    robot.dStraight(100);
    sleep(3);
    std::cout << "idling \n\r";
    robot.dStraight(0);
    std::cout << "turn me off \n\r";
}

void null_fcn(){
    std::cout << "executed null function \n\r";
}
