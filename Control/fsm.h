#ifndef FSM
#define FSM

#include "taskFunctions.h"

//typedef enum {startBot, hitWall, wallCrawl, foundExit} state;
//typedef enum {startBot, turning, getParallel, wallCrawl, leaving, out} state;
typedef enum {startBot, turning, wallCrawl, leaving, out} state;
//typedef enum {parallelFlag, onlyFarRight, front_right, front_NOTright, NOTfront_right, NOTfront_NOTright, timeoutFlag, eol} key;
typedef enum {bothLeft, onlyFarRight, front_right, front_NOTright, NOTfront_right, NOTfront_NOTright, timeoutFlag, eol} key;

typedef void (*task_fcn_ptr)();

typedef struct {
    key keyval;
    state next_state;
    task_fcn_ptr tf_ptr;
} transition;

void fsm(state ps, key keyval);

#endif // FSM

